import webbrowser
from SheetFile import SheetFile
import tkinter as tk
from tkinter import HORIZONTAL, ttk
from tkinter import LabelFrame
from tkcalendar import DateEntry
import re
import Constants
import time
import threading

class TransactionData():

    date = None
    price = None
    desc = None
    category = None

    def __init__(self, date, price, description, category):
        self.date = date
        if price != "":
            self.price = price
        if description != "":
            self.desc = description
        if category != "":
            self.category = category

class App(tk.Tk):

    SUCCESS_STYLE = 'SUCCESS.TLabel'

    def __init__(self, dev_flag):
        super().__init__()
        self.title('Transaction entry')

        self.dev_flag = dev_flag
        self.transactionVar = tk.StringVar(self, Constants.EXPENSE_LABEL, "")
        self.transactionVar.trace_add("write", self.UpdateCurrentTransaction)
        self.selectedDate = tk.StringVar()
        self.priceEntry = tk.StringVar()
        self.priceEntry.trace_add("write", self.CheckPriceEmpty)
        self.selectedCategory = tk.StringVar()

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)

        self.style = ttk.Style(self)
        self.style.configure(self.SUCCESS_STYLE, foreground='green')

        self.progress = ttk.Progressbar(self, orient=HORIZONTAL, length=100, mode='determinate')
        self.progress.grid(row=5)

        # Start a thread to perform the long-running operation
        self.sheet = None
        threading.Thread(target=self.long_running_operation).start()

        self.CreateWidgets()

    def long_running_operation(self):
        # Simulate a long-running operation
        time.sleep(5)
        self.sheet = SheetFile(self.dev_flag)

        # Update progress bar
        self.progress.stop()
        self.progress.grid_forget()

        # Schedule an update to the widgets after the long-running operation completes
        self.after(100, self.CreateWidgets)

    def CreateTransactionTypeWidgets(self, parent):
        for i, val in enumerate([Constants.EXPENSE_LABEL, Constants.REVENUE_LABEL]):
            ttk.Radiobutton(
                parent,
                text=val,
                value=val,
                variable=self.transactionVar,
                state=tk.NORMAL if self.sheet != None else tk.DISABLED).grid(row=0, column=i)

    def CreateLastTransactionWidgets(self, parent):
        # Date
        dateLabel = ttk.Label(parent, text="Date: ")
        dateLabel.grid(row=0, column=0)
        previousDateLabel = ttk.Label(parent, text="")
        previousDateLabel.grid(row=0, column=1)

        # Price
        priceLabel = ttk.Label(parent, text="Price: ")
        priceLabel.grid(row=0, column=2)
        # Allows only numbers in price
        def validatePriceEntry(newStr):
            regex = r'(-)?([0-9]+)([,|.]?[0-9]{0,2})?'
            return re.fullmatch(regex, newStr) != None

        reg = self.register(validatePriceEntry)
        self.price = tk.Entry(
            parent,
            width=7,
            textvariable=self.priceEntry,
            validate='key',
            validatecommand=(reg, '%P'))
        self.price.grid(row=0, column=3)

        # Description
        descLabel = ttk.Label(parent, text="Description: ")
        descLabel.grid(row=0, column=4)
        self.desc = tk.Entry(parent, width=22)
        self.desc.grid(row=0, column=5)

        # Category
        categLabel = ttk.Label(parent, text="Category: ")
        categLabel.grid(row=0, column=6)
        self.categ = ttk.Combobox(parent, width=20, textvariable=self.selectedCategory)
        if self.sheet is not None:
            self.categ['values'] = self.sheet.transactionCategories[self.transactionVar.get()]
        self.categ.grid(row=0, column=7)

    def CreateTransactionDetailsWidgets(self, parent):
        # Date
        dateLabel = ttk.Label(parent, text="Date: ")
        dateLabel.grid(row=0, column=0)
        self.cal = DateEntry(parent, selectmode='day', textVariable=self.selectedDate)
        self.cal.grid(row=0, column=1)

        # Price
        priceLabel = ttk.Label(parent, text="Price: ")
        priceLabel.grid(row=0, column=2)
        # Allows only numbers in price
        def validatePriceEntry(newStr):
            regex = r'((-)?([0-9]+)([,|.]?[0-9]{0,2})?)?'
            return re.fullmatch(regex, newStr) != None

        reg = self.register(validatePriceEntry)
        self.price = tk.Entry(
            parent,
            width=7,
            textvariable=self.priceEntry,
            validate='key',
            validatecommand=(reg, '%P'))
        self.price.grid(row=0, column=3)

        # Description
        descLabel = ttk.Label(parent, text="Description: ")
        descLabel.grid(row=0, column=4)
        self.desc = tk.Entry(parent, width=22)
        self.desc.grid(row=0, column=5)

        # Category
        categLabel = ttk.Label(parent, text="Category: ")
        categLabel.grid(row=0, column=6)
        self.categ = ttk.Combobox(parent, width=20, textvariable=self.selectedCategory)
        if self.sheet is not None:
            self.categ['values'] = self.sheet.transactionCategories[self.transactionVar.get()]
        self.categ.grid(row=0, column=7)

    def CreateWidgets(self):
        if self.sheet is not None:
            sheetLinkLabel = tk.Label(
                self,
                text="Click here to open Google sheet",
                fg="blue",
                cursor='hand2',
                font='Helvetica 12 bold')
            sheetLinkLabel.grid(row=0)
            sheetLinkLabel.bind("<Button-1>", lambda e : webbrowser.open_new_tab(self.sheet.GetUrl()))

        padding = {"padx": 5, "pady": 5, "sticky": tk.W}
        transactionTypeFrame = LabelFrame(self, text="Transaction type", font='Helvetica 12 bold')
        transactionTypeFrame.grid(row=1, column=0, columnspan= 2, **padding)
        self.CreateTransactionTypeWidgets(transactionTypeFrame)

        # lastTransactionFrame = LabelFrame(self, text="Last transaction", font='Helvetica 12 bold')
        # lastTransactionFrame.grid(row=2, column=0, columnspan= 2, **padding)
        # self.CreateLastTransactionWidgets(lastTransactionFrame)

        transactionDetailsFrame = LabelFrame(self, text="Transaction details", font='Helvetica 12 bold')
        transactionDetailsFrame.grid(row=3, column=0, columnspan= 2, **padding)
        self.CreateTransactionDetailsWidgets(transactionDetailsFrame)

        # Validation Button
        self.valid_button = ttk.Button(self, text="Accept", command=self.UpdateColumn, state=tk.DISABLED)
        self.valid_button.grid(row=4, column=0, **padding)
        self.validMessageLabel = ttk.Label(self, font='Helvetica 10 bold')
        self.validMessageLabel.grid(row=4, column=1, sticky=tk.E)

        # Balance Button
        def callback(event):
            txt = self.sheet.GetBalance() if event.type == tk.EventType.ButtonPress else "- ---,--"
            balanceLabel.config(text=txt)
        balanceButton = tk.Button(self, text="Display balance")
        balanceButton.bind('<ButtonPress-1>', callback)
        balanceButton.bind('<ButtonRelease-1>', callback)
        balanceButton.grid(row=5, column=0, **padding)
        balanceLabel = ttk.Label(self, font='Helvetica 10 bold', text="- ---,--")
        balanceLabel.grid(row=5, column=1, sticky=tk.W)

    def UpdateColumn(self):
        transactionData = TransactionData(
            str(self.cal.get_date()),
            self.price.get(),
            self.desc.get(),
            self.selectedCategory.get())

        transactionType = self.transactionVar.get()
        addedRow = self.sheet.AddTransaction(transactionType, transactionData)

        message = "New " + transactionType + " added Row " + str(addedRow)
        self.validMessageLabel.config(text=message)
        self.validMessageLabel['style'] = self.SUCCESS_STYLE
        self.ClearEntries()

    def ClearEntries(self):
        self.price.delete(0, tk.END)
        self.desc.delete(0, tk.END)

    def CheckPriceEmpty(self, *args):
        if self.priceEntry.get() != "" and self.sheet != None:
            state = tk.NORMAL
            self.validMessageLabel.config(text="")
        else:
            state = tk.DISABLED
        self.valid_button["state"] = state

    def UpdateCurrentTransaction(self, *args):
        self.categ['values'] = self.sheet.transactionCategories[self.transactionVar.get()]