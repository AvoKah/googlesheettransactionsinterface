import Automatisation
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='ddTrainer.py')
    parser.add_argument('-d', '--dev', help="Use this flag if you're in developper mode (we use a smaller sheet)", action="store_true")

    args = parser.parse_args()

    app = Automatisation.App(args.dev)
    app.mainloop()