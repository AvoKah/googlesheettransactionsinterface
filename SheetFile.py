import ezsheets
from datetime import date
import Constants

class SheetFile():

    def __init__(self, dev_flag):
        TEST_SHEET     = '1QhcfsxHvrD5kx7o4VTF4Xk-Pv1C7VgpzPXtL3KGCTS0'
        TEST_SHEET_TMP = '13cBhQILDNP_aeDHSjAWXfRohjNyRFHtTCn-cSAp2ZRI'
        MAIN_SHEET     = '1Z6cC7UhcnbQoPbTRkqBWuxBHmnK5uoBbwJH5OToRzOk'
        if dev_flag:
            self.spreadSheet = ezsheets.Spreadsheet(TEST_SHEET)
            self.transactionSheet = self.spreadSheet["Transactions"]
            self.recapSheet = self.spreadSheet["Récapitulatif"]
        else:
            self.spreadSheet = ezsheets.Spreadsheet(MAIN_SHEET)
            mmyy = date.today().strftime("%m/%y")
            self.transactionSheet = self.spreadSheet["Transac " + mmyy]
            self.recapSheet = self.spreadSheet["Recap " + mmyy]
        self.transactionCategories = {
            Constants.EXPENSE_LABEL : [],
            Constants.REVENUE_LABEL : []
        }

        self.FetchCategories()

    def GetNextAvailableLine(self, transactionType):
        i = ord(Constants.TRANSACTION_COLUMNS[transactionType]) - 64
        j = Constants.FIRST_AVAILABLE_ENTRY_ROW
        while (self.transactionSheet[i, j] != "" or
               self.transactionSheet[i + 1, j] != "" or
               self.transactionSheet[i + 2, j] != "" or
               self.transactionSheet[i + 3, j] != ""):
            j += 1

        return j

    def AddTransaction(self, transactionType, transactionData):
        i = ord(Constants.TRANSACTION_COLUMNS[transactionType]) - 64
        j = self.GetNextAvailableLine(transactionType)

        self.transactionSheet[i, j] = transactionData.date
        self.transactionSheet[i + 1, j] = transactionData.price
        self.transactionSheet[i + 2, j] = transactionData.desc
        self.transactionSheet[i + 3, j] = transactionData.category

        return j

    def FetchCategories(self):
        for i in range (28, 49):
            if i < 36:
                revenueCateg = self.recapSheet[Constants.RECAP_COLUMNS[Constants.REVENUE_LABEL] + str(i)]
                if (revenueCateg != ""):
                    self.transactionCategories[Constants.REVENUE_LABEL].append(revenueCateg)
            expenseCateg = self.recapSheet[Constants.RECAP_COLUMNS[Constants.EXPENSE_LABEL] + str(i)]
            if (expenseCateg != ""):
                self.transactionCategories[Constants.EXPENSE_LABEL].append(expenseCateg)

    def GetUrl(self):
        return self.spreadSheet.url

    def GetBalance(self):
        return self.recapSheet["E17"]